import React from 'react'
import './App.css';
import {BrowserRouter as Router,Link} from 'react-router-dom';

function App() {
  return (
    <Router>
      <ul>
        <li><Link to="/Connexion">Connexion</Link></li>
        <li><Link to="/Inscription">Inscription</Link></li>
      </ul>
    </Router>
    
    );
}

export default App;
