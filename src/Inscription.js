import {Form,Field} from 'react-final-from'
function Inscription(){
    return (
        <div>
        <h1>Créer un compte</h1>
        <Form onSubmit={props.submitForm}
          render={(submitInscription)=> (
            <form onSubmit={submitInscription}>
              <label>Pseudo : </label>
              <Field
                name="pseudoUser"
                type="text"
                component="input"
                initialValue={}
              />
  
              <label>Mot de passe : </label>
              <Field
                name="motDePasse"
                type="number"
                component="input"
                initialValue={}
              />
  
              <button type="submit">Se Connecter</button>
            </form>
          )}
        />  
      </div>    

    )
}
export default Inscription